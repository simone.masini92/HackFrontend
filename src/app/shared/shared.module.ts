import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipeModule } from './pipes/pipe.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DirectiveModule } from './directives/directive.module';
import { ValidatorModule } from './validators/validator.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    PipeModule,
    CommonModule,
    DirectiveModule,
    ValidatorModule,
    FlexLayoutModule,
    FontAwesomeModule,
  ],
  exports: [
    PipeModule,
    CommonModule,
    DirectiveModule,
    ValidatorModule,
    FlexLayoutModule,
    FontAwesomeModule,
    ReactiveFormsModule,
  ],
})
export class SharedModule { }
