import { NgModule } from '@angular/core';
import { EqualToValidator } from './equal-to/equal-to.directive';

const CUSTOM_FORM_DIRECTIVES = [
  EqualToValidator,
];

@NgModule({
  declarations: [CUSTOM_FORM_DIRECTIVES],
  exports: [CUSTOM_FORM_DIRECTIVES],
})
export class ValidatorModule {
}
