import { equalTo } from './equal-to/equal-to.validator';

export const CUSTOM_VALIDATOR = {
  equalTo,
};
