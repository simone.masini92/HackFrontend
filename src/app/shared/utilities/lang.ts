export const isPresent = (obj: any): boolean => obj !== undefined && obj !== null;

export const isDate = (obj: any): boolean => !/Invalid|NaN/.test(new Date(obj).toString());
