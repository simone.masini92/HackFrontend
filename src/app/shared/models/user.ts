export interface User {
  id: string;
  token: string;
  email: string;
  username: string;
  image: string;
  temporary: boolean;
  changePassword: boolean;
}

export interface UserToSignIn {
  username: string;
  password: string;
}

export interface UserToSignUp extends UserToSignIn {
  fullname: string;
  email: string;
  url: string;
}
