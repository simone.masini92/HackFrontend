import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from '@core';
import { User } from '@model';
import { faAngleDown, faCog, faEdit, faEllipsisV, faListUl, faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons/faSignOutAlt';
import { faKey } from '@fortawesome/free-solid-svg-icons/faKey';

@Component({
  selector: 'ce-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  user: User;
  faList = faEllipsisV;
  faArrow = faAngleDown;
  faSettings = faCog;
  faExit = faSignOutAlt;
  faBasket = faShoppingCart;
  faCatalog = faListUl;
  faKey = faKey;
  faSvg = faEdit;
  locale: string;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    ) {
    this.locale = window.navigator.language.substring(3, 5);
  }

  ngOnInit(): void {
    this.user = this.authenticationService.currentUser;
  }

  logout(): void {
    this.authenticationService.logout()
      .subscribe();
  }

  goToUserSettings(): void {
    this.router.navigateByUrl('/pages/user/user-settings');
  }

  changePassword(): void {
    this.router.navigate(
      ['/auth/change-password'],
      { queryParams: { returnUrl: this.router.routerState.snapshot.url, username: this.authenticationService.currentUser.username } },
      );
  }

}
