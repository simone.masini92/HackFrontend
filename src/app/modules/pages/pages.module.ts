import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { SharedModule } from '@shared';
import { HeaderModule } from '@component';

@NgModule({
  imports: [
    SharedModule,
    HeaderModule,
    PagesRoutingModule,
    DashboardModule,
  ],
  declarations: [PagesComponent],
})

export class PagesModule {
}
