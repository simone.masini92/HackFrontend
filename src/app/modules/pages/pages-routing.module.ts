import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard, ApplicationAccessGuard } from '@core';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate: [AuthGuard, ApplicationAccessGuard],
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: 'catalog',
      loadChildren: () => import('src/app/modules/pages/catalog/catalog.module').then(m => m.CatalogModule),
    },
    {
      path: 'user',
      loadChildren: () => import('src/app/modules/pages/user/user.module').then(m => m.UserModule),
    },
    {
      path: 'basket',
      loadChildren: () => import('src/app/modules/pages/basket/basket.module').then(m => m.BasketModule),
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
