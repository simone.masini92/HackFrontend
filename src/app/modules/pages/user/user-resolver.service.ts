import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { ResourceService, AuthenticationService } from '@core';
import { Observable, forkJoin } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class UserSettingsResolver implements  Resolve<any> {

  constructor(private resourceService: ResourceService, private authService: AuthenticationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.resourceService.get(this.authService.currentUser.id, 'v1/users/user-person');
  }
}

@Injectable()
export class UserSettingsDynamicResolver implements Resolve<any> {

  constructor(private resourceService: ResourceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    const user = this.resourceService.get('User', 'v1/setups/single')
      .pipe(switchMap((setups) => this.resourceService.post(setups.propertiesToSelect, 'v1/users/dynamic')));

    const person = this.resourceService.get('Person', 'v1/setups/single')
      .pipe(switchMap((setups) => this.resourceService.post(setups.propertiesToSelect, 'v1/users/persons/dynamic')));

    const company = this.resourceService.get('Company', 'v1/setups/single')
      .pipe(switchMap((setups) => this.resourceService.post(setups.propertiesToSelect, 'v1/users/companies/dynamic')));

    return forkJoin([user, person, company]);
  }
}
