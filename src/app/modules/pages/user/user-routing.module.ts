import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '@core';
import { UserComponent } from './user.component';
import { UserSettingsDynamicResolver, UserSettingsResolver } from './user-resolver.service';

const routes: Routes = [{
  path: '',
  component: UserComponent,
  canActivate: [AuthGuard],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [UserSettingsResolver, UserSettingsDynamicResolver],
})
export class UserRoutingModule {
}
