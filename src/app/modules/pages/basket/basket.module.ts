import { NgModule } from '@angular/core';
import { BasketComponent } from './basket.component';
import { BasketRoutingModule } from './basket-routing.module';

@NgModule({
  imports: [
    BasketRoutingModule,
  ],
  declarations: [BasketComponent],
})

export class BasketModule {
}
