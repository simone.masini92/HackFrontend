import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { BasketComponent } from './basket.component';
import { AuthGuard } from '@core';

const routes: Routes = [{
  path: '',
  component: BasketComponent,
  canActivate: [AuthGuard],
  // children: [
  //   {
  //     path: '',
  //     redirectTo: 'basket-dashboard',
  //     pathMatch: 'full',
  //   },
  // ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BasketRoutingModule {
}
