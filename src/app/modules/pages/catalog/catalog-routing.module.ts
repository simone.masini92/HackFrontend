import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CatalogComponent } from './catalog.component';
import { AuthGuard } from '@core';

const routes: Routes = [{
  path: '',
  component: CatalogComponent,
  canActivate: [AuthGuard],
  // children: [
  //   {
  //     path: '',
  //     redirectTo: 'catalog-dashboard',
  //     pathMatch: 'full',
  //   },
  // ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogRoutingModule {
}
