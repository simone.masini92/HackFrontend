import { NgModule } from '@angular/core';
import { CatalogComponent } from './catalog.component';
import { CatalogRoutingModule } from './catalog-routing.module';

@NgModule({
  imports: [
    CatalogRoutingModule,
    // CatalogDashboardModule,
  ],
  declarations: [CatalogComponent],
})

export class CatalogModule {
}
