import { Component, OnInit } from '@angular/core';
import { AuthenticationService, ConfigService } from '@core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '@model';
import { AuthService } from '../auth.service';
import { faGoogle } from '@fortawesome/free-brands-svg-icons/faGoogle';
import { environment } from '@environment/environment.dev';

@Component({
  selector: 'ce-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  returnUrl: string;
  loginForm: FormGroup;
  windoz;
  faGoogle = faGoogle;

  constructor(
    private _authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
  ) {
    // this.authService.startConnection();
    // this.authService.listener();
    this.authService.subscriptsable.subscribe((value) => {
      this.windoz.close();
      if (value !== undefined) {
        this.loginCallback(value);
      }
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
    this.route.queryParams.subscribe((params) => {
      const toReload = params['reload'] === 'true';
      if (toReload) {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            reload: false,
          },
        });
        setTimeout(() => {
          if (environment.production) {
            location.reload();
          }
        });
      }
    });
  }

  async submit(): Promise<void> {
    const user = await this._authenticationService.login(this.loginForm.value).toPromise();
    this.loginCallback(user);
  }

  googleSignIn(): void {
    const width = 700;
    const height = 500;
    const left = (window.screen.width - width) / 2;
    const top = (window.screen.height - height) / 2;
    this.windoz = window.open(
      `${ConfigService.settings.apiUrl}/v1/users/auth/google-sign-in/${this.authService.connectionId}`,
      'null',
      `width=${width},height=${height},left=${left},top=${top}`,
    );
  }

  private loginCallback(user: User): void {
    if (user.changePassword) {
      this.router.navigate(
        ['/auth/change-password'],
        {queryParams: {returnUrl: this.router.routerState.snapshot.url, force: true, username: this.loginForm.value.username}},
      );
    } else {
      this.router.navigate([this.returnUrl]);
    }
  }

}
