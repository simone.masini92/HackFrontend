import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ce-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss'],
})
export class VerifyEmailComponent implements OnInit {

  userId: string;
  code: string;
  text: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private router: Router,
    private toastr: ToastrService,
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.userId = params.userId;
      this.code = (params.code as string).replace(' ', '+');
    });
  }

  ngOnInit(): void {
    if (!this.userId || !this.code) {
      this.router.navigateByUrl('/auth/login');
    }
    this.authenticationService.confirmEmail(this.userId, this.code)
      .subscribe((result) => {
        this.text = 'Email confirmed!';
        this.redirect();
      }, (err) => {
        this.text = 'Email not confirmed :(';
        this.redirect();
      });
  }

  redirect(): void {
    this.toastr.info('Redirect to login in 5 seconds...').onHidden
      .subscribe(() => {
      this.router.navigateByUrl('/auth/login');
    });
  }

}
