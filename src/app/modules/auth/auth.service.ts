import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { Subject } from 'rxjs';
import { ConfigService, AuthenticationService } from '@core';
import { User } from '@model';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthService {

  private hubConnection: signalR.HubConnection;
  private elementsSource = new Subject<User>();
  subscriptsable = this.elementsSource.asObservable();
  connectionId: string;

  constructor(private authenticationService: AuthenticationService, private toastr: ToastrService) { }

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(
        `${ConfigService.settings.apiUrl}/userSignalR/auth`,
        { skipNegotiation: true, transport: signalR.HttpTransportType.WebSockets },
      )
      .build();

    this.connect();
    this.hubConnection.onclose(() => this.connect());
  };

  public listener = () => {
    this.hubConnection.on('sign-in', (user: User) => {
      this.elementsSource.next(this.authenticationService.auth(user));
    });
    this.hubConnection.on('error', (error) => {
      console.log(error);
      this.toastr.error(error);
      this.elementsSource.next(undefined);
      throw error;
    });
  };

  private connect(): void {
    this.hubConnection
      .start()
      .then(async () => {
        this.connectionId = await this.hubConnection.invoke('getConnectionId');
      })
      .catch((err) => {
        console.log(`Error while starting connection: ${err}, retry in 3 seconds`);
        this.connect();
      });
  }
}
