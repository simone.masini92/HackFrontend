import { Component, OnInit } from '@angular/core';
import { faLock, faUser } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ce-change-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {

  userId: string;
  code: string;
  confirmPassword: string;
  newPassword: string;
  username: string;
  faUser = faUser;
  faLock = faLock;

  constructor(
    private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.userId = params.userId;
      this.code = (params.code as string).replace(' ', '+');
    });
    this.confirmPassword = '';
    this.newPassword = '';
    this.username = '';
  }

  ngOnInit(): void { }

  onSubmit(): void {
    if (!this.passwordCheck()) {
      this.toastr.warning(`Passwords don't match 🤔`);
    }
    this.authenticationService.resetPassword(this.username, this.code, this.newPassword)
    .subscribe(() => {
      this.toastr.info(`Redirect to login in 5 seconds...`).onHidden
        .subscribe(() => {
          this.router.navigateByUrl('/auth/login');
        });
      this.toastr.success(`Password reset 🤩`);
    });
  }

  passwordCheck(): boolean {
    return this.newPassword === this.confirmPassword;
  }
}
