import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { ResourceService, AuthenticationService } from '@core';
import { Observable } from 'rxjs';

@Injectable()
export class HasHashResolver implements Resolve<boolean> {

  constructor(private resourceService: ResourceService, private authService: AuthenticationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const username = this.authService.currentUser
      ? this.authService.currentUser.username
      : route.queryParams.username;
    return this.resourceService.get(username, 'v1/users/has-hash');
  }
}
