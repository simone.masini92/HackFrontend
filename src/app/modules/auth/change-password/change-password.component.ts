import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CUSTOM_VALIDATOR } from '@validator';

@Component({
  selector: 'ce-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {

  readonly returnUrl: string;
  force: boolean;
  passwordForm: FormGroup;
  public username: string;
  hasHash: boolean;

  constructor(
    private _authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private toastr: ToastrService,
  ) {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    this.force = this.route.snapshot.queryParams.force === 'true';
    this.username = this.route.snapshot.queryParams.username;
    this.hasHash = this.route.snapshot.data[0];
  }

  public ngOnInit(): void {
    const newPassword = new FormControl('', [Validators.required]);
    const confirmPassword = new FormControl('', [Validators.required, CUSTOM_VALIDATOR.equalTo(newPassword)]);
    this.passwordForm = this.fb.group({
      oldPassword: [''],
      username: [this.username, [Validators.required]],
      newPassword,
      confirmPassword});
  }

  public submit(): void {
    if (this.passwordForm.invalid) {
      return;
    }
    this._authenticationService.changePassword(this.passwordForm.value)
      .subscribe(() => {
        this.toastr.success('Password changes successfully 😏');
        this.router.navigate([this.returnUrl]);
      });
  }

}
