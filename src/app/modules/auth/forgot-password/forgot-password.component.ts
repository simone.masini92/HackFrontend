import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '@core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ce-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {

  url: string;
  passwordForm: FormGroup;

  constructor(
    private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private fb: FormBuilder) {
    this.url = `${location.origin}${location.pathname}/#/auth/reset-password`;
  }

  ngOnInit(): void {
    this.passwordForm = this.fb.group({
      userName: ['', [Validators.required]],
    });
  }

  submit(): void {
    this.authenticationService.forgotPassword(this.passwordForm.value['userName'], this.url)
      .subscribe(() => {
        this.toastr.info(`Please check your email 🧐`);
      });
  }

}
