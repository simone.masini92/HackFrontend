import { Component, OnInit } from '@angular/core';
import { faLongArrowAltLeft } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserToSignUp } from '@model';
import { AuthenticationService } from '@core';

@Component({
  selector: 'ce-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  submitted: boolean;
  user: UserToSignUp;
  faArrowLeft = faLongArrowAltLeft;
  confirmPassword: string;

  constructor(
    private authenticationService: AuthenticationService,
    private toastr: ToastrService,
    private router: Router,
  ) {
    this.user = {
      email: '',
      password: '',
      fullname: '',
      username: '',
      url: `${location.origin}/#/auth/verify-email`,
    };
  }

  ngOnInit(): void {
    this.submitted = false;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.confirmPassword !== this.user.password) {
      this.toastr.warning(`Passwords mismatch`);
      this.submitted = false;

      return;
    }
    this.authenticationService.signUp(this.user)
      .subscribe((data) => {
          const response = data;
          this.submitted = false;
          this.toastr.info(`Please confirm your email 🙏`);
          this.toastr.success(`User ${this.user.username} created successfully 🤗`);
          this.router.navigateByUrl('/auth/login');
        },
        (error) => {
          this.submitted = false;
        });
  }
}
