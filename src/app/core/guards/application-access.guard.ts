import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class ApplicationAccessGuard implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private toastr: ToastrService,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // try {
    //   const validPermissions = this.authenticationService.permissions.filter((value: string) =>
    //     value.includes('FRONTOFFICE'));
    //   if (validPermissions.length < 1) {
    //     return this.error();
    //   }
    // } catch (e) {
    //   return this.error();
    // }

    return true;
  }

  error(): boolean {
    this.toastr.error(`You shall not pass 😡`);
    setTimeout(() => this.authenticationService.logout().subscribe(), 2000);

    return false;
  }
}
