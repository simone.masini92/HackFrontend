import { APP_INITIALIZER, NgModule, Optional, SkipSelf } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { ToastrModule } from 'ngx-toastr';
import { ConfigService } from './services/config.service';
import { AuthGuard } from './guards/auth.guard';
import { ApplicationAccessGuard } from './guards/application-access.guard';
import { BasicAuthInterceptor } from './interceptors/basic-auth.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { ResourceService } from './services/resource.service';
import { AuthenticationService } from './services/authentication.service';
import { DropTargetOptions, MineTypeEnum, NgxUploadModule } from '@wkoza/ngx-upload';

export const initializeApp = (appConfig: ConfigService): () => Promise<void> => () => appConfig.load();

const toastrModuleConfig = {
  positionClass: 'toast-bottom-right',
  timeOut: 5000,
};

const ngxDropTargetOptions: DropTargetOptions = {
  color: 'dropZoneColor',
  colorDrag: 'dropZoneColorDrag',
  colorDrop: 'dropZoneColorDrop',
  multiple: false,
  accept: [MineTypeEnum.Image_SvgXml],
};

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    AppRoutingModule,
    NgHttpLoaderModule.forRoot(),
    ToastrModule.forRoot(toastrModuleConfig),
    NgxUploadModule.forRoot(ngxDropTargetOptions),
  ],
  exports: [
    HttpClientModule,
    AppRoutingModule,
    NgHttpLoaderModule,
    ToastrModule,
    NgxUploadModule,
  ],
  providers: [
    AuthGuard,
    ConfigService,
    ResourceService,
    AuthenticationService,
    ApplicationAccessGuard,
    // { provide: ErrorHandler, useClass: SentryErrorHandler },
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true},
    {provide: APP_INITIALIZER, useFactory: initializeApp, deps: [ConfigService], multi: true},
  ],
})
export class CoreModule {
   // Make sure CoreModule is imported only by one NgModule the AppModule
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
