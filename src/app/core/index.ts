export { AuthenticationService } from './services/authentication.service';
export { ApplicationAccessGuard } from './guards/application-access.guard';
export { AuthGuard } from './guards/auth.guard';
export { ResourceService } from './services/resource.service';
export { ConfigService } from './services/config.service';

export { CoreModule } from './core.module';
