import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ResourceService {

  url: string;

  constructor(private httpClient: HttpClient) {
    this.url = ConfigService.settings.apiUrl;
  }

  post(item, endpoint: string): Observable<any> {
    return this.httpClient.post<any>(`${this.url}/${endpoint}`, item);
  }

  put(item, itemId: string, endpoint: string): Observable<any> {
    return this.httpClient.put<any>(`${this.url}/${endpoint}/${itemId}`, item);
  }

  get(id: string, endpoint: string): Observable<any> {
    return this.httpClient.get<any>(`${this.url}/${endpoint}/${id}`);
  }

  delete(id: string, endpoint: string): Observable<any> {
    return this.httpClient.delete<any>(`${this.url}/${endpoint}/${id}`);
  }

  all(endpoint: string): Observable<any> {
    return this.httpClient.get<any>(`${this.url}/${endpoint}`);
  }
}
