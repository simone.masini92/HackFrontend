import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, Subject, throwError } from 'rxjs';
import { UserToSignUp, User, UserToSignIn } from '@model';
import { ConfigService } from './config.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient, private router: Router) { }

  private subject = new Subject<User>();
  roleClaimType = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
  currentUser: User;
  permissions: string[];

  static createImage(user: User): User {
    const newUser = user;
    newUser.image = user.image
      ? `data:image/jpeg;base64,${user.image}`
      : undefined;

    return newUser;
  }

  login(userToSignIn: UserToSignIn): Observable<any> {
    return this.http.post<any>(`${ConfigService.settings.apiUrl}/v1/users/auth/sign-in`, userToSignIn)
      .pipe(map((user: User) => this.auth(user)));
  }

  signUp(userToSignUp: UserToSignUp): Observable<any> {
    return this.http.post<any>(`${ConfigService.settings.apiUrl}/v1/users/auth/sign-up`, userToSignUp);
  }

  logout(): Observable<any> {
    return this.http.post<any>(`${ConfigService.settings.apiUrl}/v1/users/auth/sign-out`, {})
      .pipe(map(() => {
        this.setUser(undefined);
        localStorage.removeItem('currentUser');
        sessionStorage.removeItem('currentUser');
        this.router.navigate(['/auth/login'], { queryParams: { reload: true } });
      }), catchError(() => {
        this.setUser(undefined);
        localStorage.removeItem('currentUser');
        sessionStorage.removeItem('currentUser');
        this.router.navigate(['/auth/login'], { queryParams: { reload: true } });

        return throwError('Logout');
      }));
  }

  confirmEmail(userId: string, code: string): Observable<any> {
    return this.http.get<any>(`${ConfigService.settings.apiUrl}/v1/users/auth/confirm-email?userId=${userId}&code=${code}`);
  }

  forgotPassword(userName: string, url: string): Observable<any> {
    return this.http.post<any>(`${ConfigService.settings.apiUrl}/v1/users/auth/forgot-password`, { userName, url });
  }

  resetPassword(username: string, token: string, password: string): Observable<any> {
    return this.http.post<any>(`${ConfigService.settings.apiUrl}/v1/users/auth/reset-password`, { username, token, password });
  }

  changePassword(passwords): Observable<any> {
    return this.http.post<any>(`${ConfigService.settings.apiUrl}/v1/users/auth/change-password`, passwords);
  }

  setUser(user: User): void {
    this.currentUser = user;
    if (user) {
      this.setJwtProperties(user);
    }
    this.subject.next(this.currentUser);
  }

  setJwtProperties(user: User): void {
    const jwtHelper = new JwtHelperService();
    const decodedToken = jwtHelper.decodeToken(user.token);
    this.permissions = decodedToken[this.roleClaimType];
  }

  setUserImage(image: string): void {
    this.currentUser.image = `data:image/jpeg;base64,${image}`;
    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
  }

  auth(user: User): User {
    sessionStorage.removeItem('currentUser');
    localStorage.removeItem('currentUser');

    let signInUser = user;
    if (signInUser.changePassword) {
      return signInUser;
    }

    if (signInUser) {
      signInUser = AuthenticationService.createImage(signInUser);

      // SE E' UN LOGIN TEMPORANEO SALVO L'UTENTE NEL SESSION STORAGE
      signInUser.temporary
        ? sessionStorage.setItem('currentUser', JSON.stringify(signInUser))
        : localStorage.setItem('currentUser', JSON.stringify(signInUser));
      this.setUser(signInUser);
    }
    return signInUser;
  }
}
