import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService, private toastr: ToastrService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(catchError((err) => {
        if (err.status === 401) {
          // auto logout if 401 response returned from api
          this.authenticationService.logout()
            .subscribe();
        }

        const errs = err.error;
        if (errs && errs.length > 0) {
          for (const er of errs) {
            this.toastr.error(er.description);
          }
        } else if (errs && errs.developerMeesage) {
          this.toastr.error(errs.developerMeesage.Message);
        } else {
          const error = err.message || err.statusText;
          this.toastr.error(error);
        }
        throw new Error(err.error);
    }));
  }
}
